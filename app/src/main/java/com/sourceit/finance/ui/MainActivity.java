package com.sourceit.finance.ui;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;

import com.sourceit.finance.R;

import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity {

    Fragment depositFrag;
    Fragment creditFrag;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        ButterKnife.bind(this);

        depositFrag = DepositFragment.newInstance();
        creditFrag = CreditFragment.newInstance();
    }

    @OnClick(R.id.deposit_btn)
    public void onDepositClick() {

        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.container, depositFrag)
                .commit();
    }

    @OnClick(R.id.credit_btn)
    public void onCreditClick() {

        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.container, creditFrag)
                .commit();
    }


}


