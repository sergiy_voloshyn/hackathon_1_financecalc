package com.sourceit.finance.ui;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.sourceit.finance.R;
import com.sourceit.finance.model.Credit;

import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnTextChanged;
import butterknife.Unbinder;

public class CreditFragment extends Fragment {


    Unbinder unbinder;
    Credit credit;

    @BindView(R.id.sum_credit_edit)
    EditText sumCreditEdit;

    @BindView(R.id.percent_credit_edit)
    EditText percentCreditEdit;

    @BindView(R.id.month_credit_edit)
    EditText monthCreditEdit;

    @BindView(R.id.insurance_credit_edit)
    EditText insuranceCreditEdit;

    @BindView(R.id.pre_credit_edit)
    EditText preCreditEdit;

    @BindView(R.id.tax_credit_edit)
    EditText taxCreditEdit;

    @BindView(R.id.initial_fee)
    TextView initialFeeView;

    @BindView(R.id.month_payment)
    TextView monthPaymentView;

    @BindView(R.id.overpayment)
    TextView overpaymentView;

    @BindView(R.id.total_sum)
    TextView totalSumView;


    public CreditFragment() {
        // Required empty public constructor
    }

    public static CreditFragment newInstance() {
        CreditFragment fragment = new CreditFragment();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        credit = new Credit(0, 0, 0, 0, 0, 0);
        // Inflate the layout for this fragment
        View root = inflater.inflate(R.layout.fragment_credit, container, false);
        unbinder = ButterKnife.bind(this, root);
        return root;
    }


    @Override
    public void onDestroyView() {
        unbinder.unbind();
        super.onDestroyView();
    }

    @OnTextChanged(R.id.sum_credit_edit)
    public void onTextChangeSum() {

        credit.sum = (!sumCreditEdit.getText().toString().equals("")) ? Float.valueOf(sumCreditEdit.getText().toString()) : 0;
        showChanges();
    }

    @OnTextChanged(R.id.percent_credit_edit)
    public void onTextChangePercent() {
        credit.setPercent((!percentCreditEdit.getText().toString().equals("")) ? Float.valueOf(percentCreditEdit.getText().toString()) : 0);
        showChanges();
    }

    @OnTextChanged(R.id.month_credit_edit)
    public void onTextChangeMonth() {
        credit.month = (!monthCreditEdit.getText().toString().equals("")) ? Float.valueOf(monthCreditEdit.getText().toString()) : 0;
        showChanges();
    }

    @OnTextChanged(R.id.insurance_credit_edit)
    public void onTextChangeInsurance() {
        credit.setInsurance((!insuranceCreditEdit.getText().toString().equals("")) ? Float.valueOf(insuranceCreditEdit.getText().toString()) : 0);
        showChanges();
    }

    @OnTextChanged(R.id.pre_credit_edit)
    public void onTextChangePreCredit() {
        credit.setPre((!preCreditEdit.getText().toString().equals("")) ? Float.valueOf(preCreditEdit.getText().toString()) : 0);
        showChanges();
    }

    @OnTextChanged(R.id.tax_credit_edit)
    public void onTextChangeTaxCredit() {
        credit.setTax((!taxCreditEdit.getText().toString().equals("")) ? Float.valueOf(taxCreditEdit.getText().toString()) : 0);
        showChanges();
    }


    private void showChanges() {
        initialFeeView.setText(String.format(Locale.US, "%.2f", credit.getInitialFee()));
        monthPaymentView.setText(String.format(Locale.US, "%.2f", credit.getMonthPayment()));
        overpaymentView.setText(String.format(Locale.US, "%.2f", credit.getOverPayment()));
        totalSumView.setText(String.format(Locale.US, "%.2f", credit.getTotalSum()));
    }

}
/*
1. Поле для ввода суммы кредита
2. Поле для ввода годовой ставки в %
3. Поле для ввода срока займа в месяцах
4. Поле для ввода страховки в % от суммы кредита
5. Поле для ввода авансового платежа в % от суммы кредита
6. Поле для ввода ежемесячной комиссии в % от суммы кредита
7. Текстовое поле, расчетная часть в которую входят:
7.1 Первоначальный взнос - сумма, которая должна быть выплачена сразу (поле 5)
7.2 Ежемесячный платеж - (для простоты расчетов) платеж за первый месяц по кредиту, в который входят:
часть суммы кредита (общая сумма / кол-во месяцев) + % ставка за месяц + страховка + комиссия.
7.3 Общая переплата по кредиту - сумма всех платежей по кредиту кроме самого кредита.
 А это - % по кредиту + страховка + комиссия за весь период.
7.4 Итоговая сумма погашения - вся сумма кредита + % по кредиту+ страховка + комиссия за весь период.
Все поля должны заполняться исключительно цифрами (целыми или десятичными).
 Если поле пустое - по умолчанию считается как 0.
Кнопки расчета нет, изменение любого поля влечет полный перерасчет платежей по кредиту.
 Режим отображения - исключительно портретный.

 */
