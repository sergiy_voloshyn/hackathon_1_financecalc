package com.sourceit.finance.ui;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.TextView;

import com.sourceit.finance.R;
import com.sourceit.finance.model.Deposit;

import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnCheckedChanged;
import butterknife.OnTextChanged;
import butterknife.Unbinder;

public class DepositFragment extends Fragment {

    Unbinder unbinder;
    Deposit dep;


    @BindView(R.id.sum_edit)
    EditText sumEdit;

    @BindView(R.id.percent_edit)
    EditText percentEdit;

    @BindView(R.id.month_edit)
    EditText monthEdit;

    @BindView(R.id.add_edit)
    EditText addEdit;

    @BindView(R.id.tax_edit)
    EditText taxEdit;

    @BindView(R.id.capital)
    CheckBox capitalCheckView;

    @BindView(R.id.month_income)
    TextView monthIncomeView;

    @BindView(R.id.total_income)
    TextView totalIncomeView;

    @BindView(R.id.total_month_add)
    TextView totalAddView;

    @BindView(R.id.total)
    TextView totalSumView;


    public DepositFragment() {
        // Required empty public constructor
    }

    public static DepositFragment newInstance() {
        DepositFragment fragment = new DepositFragment();

        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_deposit, container, false);
        unbinder = ButterKnife.bind(this, root);
        dep = new Deposit(0, 0, 0, 0, 0, false);
        return root;

    }

    @Override
    public void onDestroyView() {
        unbinder.unbind();
        super.onDestroyView();
    }

    @OnTextChanged(R.id.sum_edit)
    public void onTextChangeSumDep() {
        dep.sum = (!sumEdit.getText().toString().equals("")) ? Float.valueOf(sumEdit.getText().toString()) : 0;
        showChanges();
    }

    @OnTextChanged(R.id.percent_edit)
    public void onTextChangePercentDep() {
        dep.setPercent((!percentEdit.getText().toString().equals("")) ? Float.valueOf(percentEdit.getText().toString()) : 0);
        showChanges();
    }

    @OnTextChanged(R.id.month_edit)
    public void onTextChangeMonthDep() {
        dep.month = (!monthEdit.getText().toString().equals("")) ? Float.valueOf(monthEdit.getText().toString()) : 0;
        showChanges();
    }

    @OnTextChanged(R.id.add_edit)
    public void onTextChangeAddDep() {
        dep.add = (!addEdit.getText().toString().equals("")) ? Float.valueOf(addEdit.getText().toString()) : 0;
        showChanges();
    }
    @OnTextChanged(R.id.tax_edit)
    public void onTextChangeTaxDep() {
        dep.setTax((!taxEdit.getText().toString().equals("")) ? Float.valueOf(taxEdit.getText().toString()) : 0);
        showChanges();
    }

    @OnCheckedChanged(R.id.capital)
    public void onTextChangeCapitalDep(CompoundButton buttonView, boolean isChecked) {
        dep.capital = isChecked;
        Log.v("info", String.valueOf(isChecked));
        showChanges();
    }

    private void showChanges() {
        monthIncomeView.setText(String.format(Locale.US, "%.2f", dep.getMonthIncome()));
        totalIncomeView.setText(String.format(Locale.US, "%.2f", dep.getTotalIncome()));
        totalAddView.setText(String.format(Locale.US, "%.2f", dep.getTotalAdd()));
        totalSumView.setText(String.format(Locale.US, "%.2f", dep.getTotalSum()));
    }

}
/*
1. Поле для ввода суммы вклада
2. Поле для ввода годовой ставки в %
3. Поле для ввода срока размещения депозита в месяцах
4. CheckBox, который указывает, прибавляются ли к телу депозита начисленные проценты или нет.
5. Поле для ввода ежемесячных пополнений депозита.
6. Налог на прибыль (% от начисленных процентов по вкладу)
7. Текстовое поле, расчетная часть в которую входят:
7.1 Ежемесячный доход - доход по депозиту за вычетом налога и без учета капитализации и пополнений.
7.2 Общий доход - итоговый чистый доход за весь период.
7.3 Общая сумма пополнений - сумма всех пополнений за весь период депозита.
7.4 Итоговая сумма для получения - тело депозита + все пополнения + все начисленные проценты - налоги.
Все поля должны заполняться исключительно цифрами (целыми или десятичными). Если поле пустое - по умолчанию считается как 0.
Кнопки расчета нет, изменение любого поля влечет полный перерасчет доходности депозита. Режим отображения - исключительно портретный.

 */
