package com.sourceit.finance.model;


public class Deposit {
    public float sum;
    private float percent;
    public float month;
    public float add;
    private float tax;
    public boolean capital;

    public Deposit(float sum, float percent, float month, float add, float tax, boolean capital) {
        this.sum = sum;
        setPercent(percent);
        this.month = month;
        this.add = add;
        setTax(tax);
        this.capital = capital;
    }

    public float getMonthIncome() {
        if (month == 0) {
            return 0;
        }
        return sum * percent * tax / month;
    }

    public float getTotalIncome() {
        return getTotalSum() - sum - add * (month - 1);
    }

    public float getTotalAdd() {
        return add * (month - 1);
    }

    public float getTotalSum() {
        if (capital) {
            float total = sum + getMonthIncome();
            for (int i = 1; i < month; i++) {
                total += (add + (total + add) * percent) * tax;
            }
            return total;
        }
        return (sum * tax + getMonthIncome() * month) + add * (month - 1) * (1 + percent) * tax;
    }

    public void setPercent(float percent) {
        this.percent = percent / 100;
    }

    public void setTax(float tax) {
        this.tax = 1 - tax / 100;
    }
}
